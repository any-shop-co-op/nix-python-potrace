# Contributing to nix-python-potrace

Thank you for your interest in contributing to `nix-python-potrace`! Your contributions are valuable to the project's success. Whether you're suggesting enhancements, reporting bugs, or submitting a pull request, we appreciate your effort to make this project better.

## Quick Links

- [Project Issues](https://gitlab.com/any-shop-co-op/nix-python-potrace/issues)

## How to Contribute

### Reporting Issues

If you encounter a problem or have a suggestion for improvement, please check the [issue tracker](https://gitlab.com/any-shop-co-op/nix-python-potrace/issues) first to see if your concern has already been addressed. If not, feel free to open a new issue. Provide as much context as you can, including steps to reproduce the issue if applicable.

### Suggesting Enhancements

We welcome ideas for making `nix-python-potrace` better. If you have a suggestion for enhancing the project, please open an issue to discuss it. Be specific about what you'd like to see and why it would be an improvement.

### Submitting Changes

If you'd like to contribute directly to the project, you can fork the repository, make your changes, and submit a merge request. While we don't have strict coding conventions, we ask that you:

- Keep your changes focused. If you have multiple unrelated changes, please submit them as separate merge requests.
- Update or add relevant documentation as needed.
- Verify that your changes work with the Nix infrastructure.

### Testing Your Changes

We encourage you to test your changes thoroughly to ensure they work as expected and don't introduce new issues. If your changes affect how users interact with `nix-python-potrace`, include that information in your merge request description.

## Questions?

If you have any questions or need further clarification about contributing, feel free to open an issue for discussion. We aim to foster a welcoming and collaborative environment and expect all contributors to respect each other.

Thank you for contributing to `nix-python-potrace`!