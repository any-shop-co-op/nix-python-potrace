# nix-python-potrace

This repository provides Nix expressions for building and developing the `python-potrace` package within Nix environments. It supports both traditional Nix and Nix Flakes workflows.

## CI/CD Status

![Pipeline status](https://gitlab.com/<YOUR_NAMESPACE>/nix-python-potrace/badges/main/pipeline.svg)
![Coverage report](https://gitlab.com/<YOUR_NAMESPACE>/nix-python-potrace/badges/main/coverage.svg)

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

- [Nix Package Manager](https://nixos.org/download.html)
- Optional: [Nix Flakes](https://nixos.wiki/wiki/Flakes) support if you want to use the flake workflow.

### Using with Nix Flakes

To use `nix-python-potrace` with Nix Flakes, you can enter a development shell with all dependencies available:

```sh
nix develop github:yourusername/nix-python-potrace
```

Or build it directly:

```sh
nix build github:yourusername/nix-python-potrace
```

### Using without Nix Flakes

If you are not using Nix Flakes, you can still use the traditional `nix-shell` to enter a development environment:

```sh
nix-shell
```

And build the package using `nix-build`:

```sh
nix-build
```

## Running the Tests

Explain how to run the automated tests for this system:

```sh
nix-build ./tests  # For non-flake users
nix build .#checks  # For flake users
```

## Contributing

Please read [CONTRIBUTING.md](CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests to us.

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.

## Acknowledgments

- ChatGPT