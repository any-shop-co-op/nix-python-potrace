{ fetchPypi, buildPythonPackage, lib }:

let
  # Define the pypotrace package
  pypotrace = buildPythonPackage rec {
    pname = "pypotrace";
    version = "0.3";

    src = fetchPypi {
      inherit pname version;
      sha256 = "0000000000000000000000000000000000000000000000000000";
    };

    propagatedBuildInputs = [ ];

    meta = {
      homepage = "https://pythonhosted.org/pypotrace/";
      license = lib.licenses.mit;
      description = "Python bindings for the potrace library";
    };
  };

in
{
  # Export the package for external use
  inherit pypotrace;
}