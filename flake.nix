{
  description = "A Nix flake for the python-potrace package";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs {
          inherit system;
        };

        python-potrace = pkgs.callPackage ./pkgs/python-potrace.nix { };
      in
      {
        packages.python-potrace = python-potrace;

        devShell = pkgs.mkShell {
          buildInputs = [ python-potrace pkgs.git ];
        };

        checks = {
          inherit (python-potrace) tests;
        };
      }
    );
}