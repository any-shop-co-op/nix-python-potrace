{ lib, buildPythonPackage, fetchPypi }:

buildPythonPackage rec {
  pname = "pypotrace";
  version = "0.3";

  src = fetchPypi {
    inherit pname version;
    sha256 = "0000000000000000000000000000000000000000000000000000";
  };

  propagatedBuildInputs = [ ];

  meta = {
    homepage = "https://pythonhosted.org/pypotrace/";
    license = lib.licenses.mit;
    description = "Python bindings for the potrace library";
  };
}