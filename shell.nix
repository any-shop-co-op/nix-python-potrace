{ pkgs ? import <nixpkgs> {} }:

pkgs.mkShell {
  buildInputs = [
    pkgs.python3
    pkgs.python3Packages.pip
    pkgs.git
    pkgs.nixFlakes
  ];

  # Set PYTHONPATH to include the python-potrace package directory.
  shellHook = ''
    export PYTHONPATH=$PWD/pkgs:$PYTHONPATH
  '';
}