{ nixpkgs ? import <nixpkgs> {}, python-potrace }:

let
  inherit (nixpkgs) stdenv;
in
stdenv.mkDerivation {
  name = "nix-python-potrace-test";

  buildInputs = [ python-potrace ];

  buildCommand = ''
    echo "Running basic tests for python-potrace"
    # TODO invoke any available tests or simple usage examples
    # TODO check if the module can be imported without error
    python -c "import potrace" 
    echo "Basic test completed successfully"
  '';
}